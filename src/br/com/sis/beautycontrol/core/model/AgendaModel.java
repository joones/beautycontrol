package br.com.sis.beautycontrol.core.model;

public class AgendaModel {
	
	private int id;
	private int costumerId;
	private String serviceDate;
    private String serviceHour;
    private String status;
    
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getCostumerId() {
		return costumerId;
	}
	
	public void setCostumerId(int costumerId) {
		this.costumerId = costumerId;
	}
	
	public String getServiceDate() {
		return serviceDate;
	}
	
	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}
	
	public String getServiceHour() {
		return serviceHour;
	}
	
	public void setServiceHour(String serviceHour) {
		this.serviceHour = serviceHour;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
    
}
