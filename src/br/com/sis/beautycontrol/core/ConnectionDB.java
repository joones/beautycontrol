package br.com.sis.beautycontrol.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {
  
	private static final String DSN  = "jdbc:postgresql://localhost/beautycontrol";
	private static final String USER = "postgres";
	private static final String PASS = "root";
		
	public static Connection connectToServer() throws Exception, SQLException {
		Class.forName("org.postgresql.Driver");
		Connection conn = DriverManager.getConnection(DSN, USER, PASS);
		return conn;
	}
	
	public void connect() {
		try {
			connectToServer();
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
