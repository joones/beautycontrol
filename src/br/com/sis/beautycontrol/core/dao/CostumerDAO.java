package br.com.sis.beautycontrol.core.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.sis.beautycontrol.core.ConnectionDB;
import br.com.sis.beautycontrol.core.model.CostumerModel;

public class CostumerDAO {
		
	private Connection conn = null;
	private PreparedStatement pstmt = null;
	private ResultSet rset = null;
	
	public void create(CostumerModel costumer){
		String sql = "INSERT INTO costumer (name, phone, cellphone, email) VALUES(?,?,?,?)";
		
		try {
			conn  = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, costumer.getName());
			pstmt.setString(2, costumer.getPhone());
			pstmt.setString(3, costumer.getCellphone());
			pstmt.setString(4, costumer.getEmail());
			
			pstmt.execute();
			
			JOptionPane.showMessageDialog(null, "Cliente registrado!", "Conclu�do", JOptionPane.INFORMATION_MESSAGE);	
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Conclu�do", JOptionPane.INFORMATION_MESSAGE);
		} finally {
			try {
				if (pstmt != null)
				pstmt.close();
				
				if (conn != null)
				conn.close();
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
	
	public List<CostumerModel> read(){
		String sql = "Select * from costumer";
		List<CostumerModel> costumers = new ArrayList<CostumerModel>();
		
		try {
			conn = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			rset  = pstmt.executeQuery();
			
			while (rset.next()) {
				CostumerModel costumerModel = new CostumerModel();
				
				costumerModel.setId(rset.getInt("id"));
				costumerModel.setName(rset.getString("name"));
				costumerModel.setPhone(rset.getString("phone"));
				costumerModel.setCellphone(rset.getString("cellphone"));
				costumerModel.setEmail(rset.getString("email"));
				
				costumers.add(costumerModel);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (rset != null)
					rset.close();
				
				if (pstmt != null)
					pstmt.close();
				
				if(conn != null)
					conn.close();
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		
		return costumers;
	}
	
	public void update (CostumerModel costumer){
		String sql = "UPDATE costumer SET name=?, phone=?, cellphone=?, email=? WHERE id=?";
		
		try {
			conn  = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, costumer.getName());
			pstmt.setString(2, costumer.getPhone());
			pstmt.setString(3, costumer.getCellphone());
			pstmt.setString(4, costumer.getEmail());
			pstmt.setInt(5, costumer.getId());
			pstmt.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null)
					pstmt.close();
				
				if(conn != null)
					conn.close();
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
	
	public void delete (int id){
		String sql  = "DELETE FROM costumer WHERE id=?";
		
		try {
			conn = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null)
					pstmt.close();
				
				if(conn != null)
					conn.close();
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

}
