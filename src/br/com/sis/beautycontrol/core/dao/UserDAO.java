package br.com.sis.beautycontrol.core.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.sis.beautycontrol.core.ConnectionDB;
import br.com.sis.beautycontrol.core.model.UserModel;

public class UserDAO {
	
	private Connection conn = null;
	private PreparedStatement pstmt = null;
	private ResultSet rset = null;

	public void create(UserModel user){
		String sql = "INSERT INTO users (name, email, password, status, created_at, updated_at) VALUES(?,?,?,?,?,?)";
		
		try {
			conn  = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, user.getName());
			pstmt.setString(2, user.getEmail());
			pstmt.setString(3, user.getPassword());
			pstmt.setInt(4, user.getStatus());
			pstmt.setString(5, user.getCreatedAt());
			pstmt.setString(6, user.getUpdatedAt());
			
			pstmt.execute();
			
			JOptionPane.showMessageDialog(null, "Usu�rio registrado!", "Conclu�do", JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Conclu�do", JOptionPane.INFORMATION_MESSAGE);
		} finally {
			try {
				if (pstmt != null) 
					pstmt.close();
				
				if (conn != null) 
					conn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
	
	public List<UserModel> read() {
		String sql = "SELECT * FROM users";
		List<UserModel> users = new ArrayList<UserModel>();
		
		try {
			conn  = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			rset  = pstmt.executeQuery();
			
			while (rset.next()) {
				UserModel userModel = new UserModel();
				
				userModel.setId(rset.getInt("id"));
				userModel.setName(rset.getString("name"));
				userModel.setEmail(rset.getString("email"));
				userModel.setStatus(rset.getInt("status"));
				userModel.setCreatedAt(rset.getString("created_at"));
				
				users.add(userModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rset != null)
					rset.close();
				
				if (pstmt != null) 
					pstmt.close();
				
				if (conn != null) 
					conn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		
		return users;
	}
	
	public void update(UserModel user) {
		String sql = "UPDATE users SET name=?, email=?, updated_at=? WHERE id=?";
		
		try {
			conn  = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, user.getName());
			pstmt.setString(2, user.getEmail());
			pstmt.setString(3, user.getUpdatedAt());
			pstmt.setInt(4, user.getId());
			pstmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null) 
					pstmt.close();
				
				if (conn != null) 
					conn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
	
	public void delete(int id) {
		String sql = "DELETE FROM users WHERE id=?";
		
		try {
			conn  =	ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null) 
					pstmt.close();
				
				if (conn != null) 
					conn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}















