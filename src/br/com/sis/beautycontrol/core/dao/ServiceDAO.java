package br.com.sis.beautycontrol.core.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.sis.beautycontrol.core.ConnectionDB;
import br.com.sis.beautycontrol.core.model.ServiceModel;

public class ServiceDAO {
	
	private Connection conn = null;
	private PreparedStatement pstmt = null;
	private ResultSet rset = null;
	
	public void create (ServiceModel service){
		String sql = "INSERT INTO service (name, price) VALUES(?,?)";
		
		try {
			conn  = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, service.getName());
			pstmt.setDouble(2, service.getPrice());
			
			pstmt.execute();
			
			JOptionPane.showMessageDialog(null, "Servi�o registrado!", "Conclu�do", JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Conclu�do", JOptionPane.INFORMATION_MESSAGE);
		} finally {
			try {
				if (pstmt != null)
				pstmt.close();
				
				if (conn != null)
				conn.close();
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
	
	public List<ServiceModel> read(){
		String sql = "SELECT * FROM service";
		List<ServiceModel> service = new ArrayList<ServiceModel>();
		
		try {
			conn = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			rset = pstmt.executeQuery();
			
			while (rset.next()){
				ServiceModel serviceModel = new ServiceModel();
				
				serviceModel.setId(rset.getInt("id"));
				serviceModel.setName(rset.getString("name"));
				serviceModel.setPrice(rset.getDouble("price"));
				
				service.add(serviceModel);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (rset != null)
				rset.close();
				
				if (pstmt != null)
				pstmt.close();
				
				if(conn != null)
				conn.close();
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		
		return service;
	}
	
	public void update (ServiceModel service){
		String sql = "UPDATE service SET name = ?, price = ? WHERE id = ?";
		
		try {
			conn = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, service.getName());
			pstmt.setDouble(2, service.getPrice());
			pstmt.setInt(3, service.getId());
			
			pstmt.execute();
			
			JOptionPane.showMessageDialog(null, "Servi�o atualizado!", "Conclu�do", JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Conclu�do", JOptionPane.INFORMATION_MESSAGE);
		}finally {
			try {
				if (pstmt != null)
				pstmt.close();
				
				if (conn != null)
				conn.close();
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
	
	public void delete (int id){
		String sql = "DELETE FROM service WHERE id=?";
		
		try {
			conn = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
			
			JOptionPane.showMessageDialog(null, "Servi�o removido!", "Conclu�do", JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Conclu�do", JOptionPane.INFORMATION_MESSAGE);
		}finally {
			try {
				if (pstmt != null)
				pstmt.close();
				
				if(conn != null)
				conn.close();
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}
