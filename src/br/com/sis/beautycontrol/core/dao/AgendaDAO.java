package br.com.sis.beautycontrol.core.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.sis.beautycontrol.core.ConnectionDB;
import br.com.sis.beautycontrol.core.model.AgendaModel;

public class AgendaDAO {
	
	private Connection conn = null;
	private PreparedStatement pstmt = null;
	private ResultSet rset = null;
	
	public void create (AgendaModel agenda){
		String sql = "INSERT INTO agenda (costumer_id, service_date, service_hour, status) VALUES(?,?,?,?)";
		
		try {
			conn = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, agenda.getCostumerId());
			pstmt.setString(2, agenda.getServiceDate());
			pstmt.setString(3, agenda.getServiceHour());
			pstmt.setString(4, agenda.getStatus());
			
			pstmt.execute();
			
			JOptionPane.showMessageDialog(null, "Hor�rio registrado!", "Conclu�do", JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Conclu�do", JOptionPane.INFORMATION_MESSAGE);
		} finally {
			try {
				
				if (pstmt != null)
				pstmt.close();
				
				if (conn != null)
					conn.close();
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
	
	public List<AgendaModel> read() {
		String sql = "SELECT * FROM agenda";
		List<AgendaModel> agenda = new ArrayList<AgendaModel>();
		
		try {
			conn = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				AgendaModel agendaModel = new AgendaModel();
				
				agendaModel.setId(rset.getInt("id"));
				agendaModel.setCostumerId(rset.getInt("costumer_id"));
				agendaModel.setServiceDate(rset.getString("service_date"));
				agendaModel.setServiceHour(rset.getString("service_hour"));
				agendaModel.setStatus(rset.getString("status"));
				
				agenda.add(agendaModel);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (rset != null)
				rset.close();
				
				if (pstmt != null)
				pstmt.close();
				
				if(conn != null)
				conn.close();
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		
		return agenda;
		
	}
	
	public void update (AgendaModel agenda){
		String sql = "UPDATE agenda SET costumer_id=?, service_date=?, service_hour=?, status=? WHERE id=?";
		
		try {
			conn = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, agenda.getCostumerId());
			pstmt.setString(2, agenda.getServiceDate());
			pstmt.setString(3, agenda.getServiceHour());
			pstmt.setString(4, agenda.getStatus());
			pstmt.setInt(5, agenda.getId());
			
			pstmt.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (pstmt != null)
				pstmt.close();
				
				if(conn != null)
				conn.close();
					
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
	
	public void delete(int id){
		String sql = "DELETE FROM agenda WHERE id=?";
		
		try {
			conn = ConnectionDB.connectToServer();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (pstmt != null)
					pstmt.close();
					
					if(conn != null)
					conn.close();	
			} catch (Exception e2) {
				e2.printStackTrace();
			}		
		}
	}
	
}