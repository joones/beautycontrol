package br.com.sis.beautycontrol.views;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import br.com.sis.beautycontrol.core.dao.UserDAO;
import br.com.sis.beautycontrol.core.model.UserModel;

@SuppressWarnings("serial")
public class SearchUserInternalFrame extends JInternalFrame {
	private JTable tableUser;
	private DefaultTableModel tableModel = new DefaultTableModel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchUserInternalFrame frame = new SearchUserInternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public SearchUserInternalFrame() {
		setTitle("Consultar usuários");
		setFrameIcon(null);
		setClosable(true);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBounds(100, 100, 775, 497);
		getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 746, 435);
		getContentPane().add(scrollPane);

		UserDAO userDAO = new UserDAO();

		tableUser = new JTable(tableModel);
		tableModel.addColumn("Nome");
		tableModel.addColumn("Email");
		tableModel.addColumn("Status");
		tableModel.addColumn("Ações");
		tableUser.getColumnModel().getColumn(0).setPreferredWidth(323);
		tableUser.getColumnModel().getColumn(1).setPreferredWidth(323);
		tableUser.getColumnModel().getColumn(2).setPreferredWidth(100);
		tableUser.getColumnModel().getColumn(2).setPreferredWidth(100);

		tableModel.setNumRows(0);
		
		for (UserModel userModel : userDAO.read()) {
			tableModel.addRow(new Object[] { userModel.getName(), userModel.getEmail(), userModel.getStatus() });
		}
		
		scrollPane.setViewportView(tableUser);

	}

}
