package br.com.sis.beautycontrol.views;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import br.com.sis.beautycontrol.core.dao.UserDAO;
import br.com.sis.beautycontrol.core.model.UserModel;

@SuppressWarnings("serial")
public class NewUserInternalFrame extends JInternalFrame {
	private JTextField jtfName;
	private JTextField jtfEmail;
	private JTextField jtfPassword;
	private JTextField jtfConfirm;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewUserInternalFrame frame = new NewUserInternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NewUserInternalFrame() {
		setNormalBounds(new Rectangle(0, 0, 300, 0));
		setFrameIcon(null);
		setTitle("Cadastrar usuário");
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setClosable(true);
		setBounds(100, 100, 724, 483);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setFont(new Font("Dialog", Font.BOLD, 13));
		lblNome.setBounds(12, 12, 535, 14);
		getContentPane().add(lblNome);
		
		jtfName = new JTextField();
		jtfName.setBounds(12, 30, 359, 30);
		getContentPane().add(jtfName);
		jtfName.setColumns(10);
		
		JLabel label = new JLabel("Email:");
		label.setFont(new Font("Dialog", Font.BOLD, 13));
		label.setBounds(12, 72, 113, 14);
		getContentPane().add(label);
		
		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setFont(new Font("Dialog", Font.BOLD, 13));
		lblSenha.setBounds(12, 134, 113, 14);
		getContentPane().add(lblSenha);
		
		JLabel lblConfirmarSenha = new JLabel("Confirmar Senha:");
		lblConfirmarSenha.setFont(new Font("Dialog", Font.BOLD, 13));
		lblConfirmarSenha.setBounds(197, 134, 217, 14);
		getContentPane().add(lblConfirmarSenha);
		
		JButton jbNew = new JButton("Cadastrar");
		jbNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		jbNew.setForeground(Color.WHITE);
		jbNew.setBackground(Color.BLACK);
		jbNew.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				UserDAO userDAO     = new UserDAO();
				UserModel userModel = new UserModel();
				Date dateNow 		= new Date();
				SimpleDateFormat sf = new SimpleDateFormat("Y-m-d H:m:s");
				
				userModel.setName(jtfName.getText());
				userModel.setEmail(jtfEmail.getText());
				userModel.setPassword(jtfPassword.getText());
				userModel.setStatus(1);
				userModel.setCreatedAt(sf.format(dateNow));
				userModel.setUpdatedAt(sf.format(dateNow));	
				
				if(jtfPassword.getText().length() < 6 ){
					JOptionPane.showMessageDialog(null, "A senha deve conter no m�nimo 6 caracteres.", "Fechar", JOptionPane.INFORMATION_MESSAGE);
					jtfConfirm.setText("");	
				} else {

					if (jtfPassword.getText().equals(jtfConfirm.getText())){			
						userDAO.create(userModel);
						
						jtfEmail.setText("");
						jtfName.setText("");
						jtfPassword.setText("");
						jtfConfirm.setText("");
						userDAO.create(userModel);
						
					} else {
						JOptionPane.showMessageDialog(null, "Confirma��o de senha n�o confere com campo senha.", "Fechar", JOptionPane.INFORMATION_MESSAGE);
						jtfConfirm.setText("");
					}
					
				}
				
			}
		});
		jbNew.setBounds(12, 201, 108, 36);
		getContentPane().add(jbNew);
		
		JButton jbReset = new JButton("Limpar");
		jbReset.setBackground(Color.BLACK);
		jbReset.setForeground(Color.WHITE);
		jbReset.setBounds(132, 201, 113, 36);
		jbReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		jbReset.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				jtfEmail.setText("");
				jtfName.setText("");
				jtfPassword.setText("");
				jtfConfirm.setText("");
			}
		});
		getContentPane().add(jbReset);
		
		jtfEmail = new JTextField();
		jtfEmail.setColumns(10);
		jtfEmail.setBounds(12, 92, 359, 30);
		getContentPane().add(jtfEmail);
		
		jtfPassword = new JPasswordField();
		jtfPassword.setColumns(10);
		jtfPassword.setBounds(12, 152, 174, 30);
		getContentPane().add(jtfPassword);
		
		jtfConfirm = new JPasswordField();
		jtfConfirm.setColumns(10);
		jtfConfirm.setBounds(197, 152, 174, 30);
		getContentPane().add(jtfConfirm);

	}
}
