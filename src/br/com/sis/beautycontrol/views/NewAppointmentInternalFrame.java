package br.com.sis.beautycontrol.views;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import br.com.sis.beautycontrol.core.dao.AgendaDAO;
import br.com.sis.beautycontrol.core.model.AgendaModel;

@SuppressWarnings("serial")
public class NewAppointmentInternalFrame extends JInternalFrame {
	private JTextField jtfTime;
	private JTextField jtfDate;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewAppointmentInternalFrame frame = new NewAppointmentInternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NewAppointmentInternalFrame() {
		setTitle("Agendar hor�rio");
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setClosable(true);
		setBounds(100, 100, 450, 300);
		setFrameIcon(null);
		getContentPane().setLayout(null);
		
		JLabel lblData = new JLabel("Data:");
		lblData.setBounds(7, 11, 36, 14);
		getContentPane().add(lblData);
		
		JLabel lblHorrio = new JLabel("Hor\u00E1rio:");
		lblHorrio.setBounds(119, 11, 61, 14);
		getContentPane().add(lblHorrio);
		
		jtfDate = new JTextField();
		jtfDate.setBounds(7, 25, 84, 23);
		getContentPane().add(jtfDate);
		jtfDate.setColumns(10);
		
		jtfTime = new JTextField();
		jtfTime.setBounds(117, 25, 117, 23);
		getContentPane().add(jtfTime);
		jtfTime.setColumns(10);
		
		JButton btnAgendar = new JButton("Agendar");
		btnAgendar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				AgendaDAO agendaDAO = new AgendaDAO();
				AgendaModel agendaModel = new AgendaModel();
				
				agendaModel.setServiceDate(jtfDate.getText());
				agendaModel.setServiceHour(jtfTime.getText());
				agendaModel.setCostumerId(1);
				agendaModel.setStatus("P");
				agendaDAO.create(agendaModel);
				
				jtfDate.setText("");
				jtfTime.setText("");
			}
		});
		btnAgendar.setBounds(147, 59, 84, 35);
		btnAgendar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		JButton btnLimparCampos = new JButton("Limpar Campos");
		btnLimparCampos.setBounds(7, 59, 130, 35);
		btnLimparCampos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				jtfTime.setText("");
				jtfDate.setText("");
			}
		});
		getContentPane().add(btnLimparCampos);
		getContentPane().add(btnAgendar);
	}
}
