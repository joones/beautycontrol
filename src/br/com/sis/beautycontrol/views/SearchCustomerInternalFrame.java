package br.com.sis.beautycontrol.views;

import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JInternalFrame;
import javax.swing.border.LineBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.com.sis.beautycontrol.core.dao.CostumerDAO;
import br.com.sis.beautycontrol.core.model.CostumerModel;

@SuppressWarnings("serial")
public class SearchCustomerInternalFrame extends JInternalFrame {
	private JTable tableCostumer;
	private DefaultTableModel tableModel = new DefaultTableModel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchCustomerInternalFrame frame = new SearchCustomerInternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SearchCustomerInternalFrame() {
		setClosable(true);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setTitle("Consultar clientes");
		setFrameIcon(null);
		setBounds(100, 100, 732, 501);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(50, 47, 690, 387);
		getContentPane().add(scrollPane);
		
		CostumerDAO costumerDAO = new CostumerDAO();
		
		tableCostumer = new JTable (tableModel);
		tableModel.addColumn("Nome");
		tableModel.addColumn("Telefone");
		tableModel.addColumn("Celular");
		tableModel.addColumn("Email");
		
		tableCostumer.getColumnModel().getColumn(0);
		tableCostumer.getColumnModel().getColumn(1);
		tableCostumer.getColumnModel().getColumn(2);
		tableCostumer.getColumnModel().getColumn(3);
		
		tableModel.setNumRows(0);
		
		for (CostumerModel costumerModel : costumerDAO.read()){
			tableModel.addRow(new Object[] { costumerModel.getName(), costumerModel.getPhone(), costumerModel.getCellphone(), costumerModel.getEmail() });
		}
		
		scrollPane.setViewportView(tableCostumer);
		
	}
}
