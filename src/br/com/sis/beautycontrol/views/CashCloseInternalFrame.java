package br.com.sis.beautycontrol.views;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class CashCloseInternalFrame extends JInternalFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CashCloseInternalFrame frame = new CashCloseInternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CashCloseInternalFrame() {
		setTitle("Fechamento de Caixa");
		setFrameIcon(null);
		setClosable(true);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);

	}
}
