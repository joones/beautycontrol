package br.com.sis.beautycontrol.views;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class MainLayout extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainLayout frame = new MainLayout();
					frame.setVisible(true);
					//frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainLayout() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnAgendamentos = new JMenu("Agendamentos");
		menuBar.add(mnAgendamentos);
		
		JMenuItem mntmAgendar = new JMenuItem("Agendar");
		mntmAgendar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NewAppointmentInternalFrame naif = new NewAppointmentInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(naif);
				naif.setVisible(true);
				naif.setLocation(0, 0);
				
				try {
					naif.setMaximum(true);
				} catch (Exception e2) {
					e2.getMessage();
				}
			}
		});
		mnAgendamentos.add(mntmAgendar);
		
		JMenuItem mntmConsultar = new JMenuItem("Consultar");
		mntmConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SearchAppointmentInternalFrame saif = new SearchAppointmentInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(saif);
				saif.setVisible(true);
				saif.setLocation(0, 0);
				
				try {
					saif.setMaximum(true);
				} catch (Exception e2) {
					e2.getMessage();
				}
			}
		});
		mnAgendamentos.add(mntmConsultar);	
		
		JMenu mnCaixa = new JMenu("Caixa");
		menuBar.add(mnCaixa);
		
		JMenuItem mntmConsultar_1 = new JMenuItem("Consultar");
		mntmConsultar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CashSearchInternalFrame csif = new CashSearchInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(csif);
				csif.setVisible(true);
				csif.setLocation(0, 0);
				
				try {
					csif.setMaximum(true);
				} catch (Exception ex) {
					ex.getMessage();
				}
			}
		});
		mnCaixa.add(mntmConsultar_1);
		
		JMenuItem mntmFechamento = new JMenuItem("Fechamento");
		mntmFechamento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CashCloseInternalFrame ccif = new CashCloseInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(ccif);
				ccif.setVisible(true);
				ccif.setLocation(0, 0);
				
				try {
					ccif.setMaximum(true);
				} catch (Exception ex) {
					ex.getMessage();
				}
			}
		});
		mnCaixa.add(mntmFechamento);
		
		JMenu mnClientes = new JMenu("Clientes");
		menuBar.add(mnClientes);
		
		JMenuItem mntmCadastrar_1 = new JMenuItem("Cadastrar");
		mntmCadastrar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NewCustomerInternalFrame ncif = new NewCustomerInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(ncif);
				ncif.setVisible(true);
				ncif.setLocation(0, 0);
				
				try {
					ncif.setMaximum(true);
				} catch (PropertyVetoException ex) {
					ex.getMessage();
				}
			}
		});
		mnClientes.add(mntmCadastrar_1);
		
		JMenuItem mntmConsultar_2 = new JMenuItem("Consultar");
		mntmConsultar_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SearchCustomerInternalFrame scif = new SearchCustomerInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(scif);
				scif.setVisible(true);
				scif.setLocation(0, 0);
				
				try {
					scif.setMaximum(true);
				} catch (PropertyVetoException ex) {
					ex.getMessage();
				}
			}
		});
		mnClientes.add(mntmConsultar_2);
		
		JMenu mnRelatrios = new JMenu("Relat\u00F3rios");
		menuBar.add(mnRelatrios);
		
		JMenuItem mntmConsultar_4 = new JMenuItem("Consultar");
		mntmConsultar_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SearchReportInternalFrame srif = new SearchReportInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(srif);
				srif.setVisible(true);
				srif.setLocation(0, 0);
				
				try {
					srif.setMaximum(true);
				} catch (Exception ex) {
					ex.getMessage();
				}
			}
		});
		mnRelatrios.add(mntmConsultar_4);
		
		JMenuItem mntmNovo = new JMenuItem("Novo");
		mntmNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NewReportInternalFrame nrip = new NewReportInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(nrip);
				nrip.setVisible(true);
				nrip.setLocation(0, 0);
				
				try {
					nrip.setMaximum(true);
				} catch (Exception ex) {
					ex.getMessage();
				}
			}
		});
		mnRelatrios.add(mntmNovo);
		
		JMenu mnServios = new JMenu("Servi\u00E7os");
		menuBar.add(mnServios);
		
		JMenuItem mntmCadastrar = new JMenuItem("Cadastrar");
		mntmCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NewServiceInternalFrame nsif = new NewServiceInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(nsif);
				nsif.setVisible(true);
				nsif.setLocation(0, 0);
		
				try {
					nsif.setMaximum(true);
				} catch (Exception ex) {
					ex.getMessage();
				}
			}
		});
		mnServios.add(mntmCadastrar);
		
		JMenuItem mntmConsultar_5 = new JMenuItem("Consultar ");
		mntmConsultar_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SearchServiceInternalFrame ssif = new SearchServiceInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(ssif);
				ssif.setVisible(true);
				ssif.setLocation(0, 0);
		
				try {
					ssif.setMaximum(true);
				} catch (Exception ex) {
					ex.getMessage();
				}
			}
		});
		mnServios.add(mntmConsultar_5);
		
		JMenuItem mntmServiosRealizados = new JMenuItem("Servi\u00E7os realizados");
		mntmServiosRealizados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}
		});
		mnServios.add(mntmServiosRealizados);
		
		JMenu mnUsurios = new JMenu("Usu\u00E1rios");
		menuBar.add(mnUsurios);
		
		JMenuItem mntmCadastrar_2 = new JMenuItem("Cadastrar");
		mntmCadastrar_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NewUserInternalFrame nuif = new NewUserInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(nuif);
				nuif.setVisible(true);
				nuif.setLocation(0, 0);
		
				try {
					nuif.setMaximum(true);
				} catch (Exception ex) {
					ex.getMessage();
				}
			}
		});
		mnUsurios.add(mntmCadastrar_2);
		
		JMenuItem mntmConsultar_3 = new JMenuItem("Consultar");
		mntmConsultar_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SearchUserInternalFrame suif = new SearchUserInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(suif);
				suif.setVisible(true);
				suif.setLocation(0, 0);
				
				try {
					suif.setMaximum(true);
				} catch (Exception ex) {
					ex.getMessage();
				}
			}
		});
		mnUsurios.add(mntmConsultar_3);
		
		JMenuItem mntmPerfil = new JMenuItem("Perfil");
		mntmPerfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserProfileInternalFrame upif = new UserProfileInternalFrame();
				
				contentPane.removeAll();
				contentPane.add(upif);
				upif.setVisible(true);
				upif.setLocation(0, 0);
				
				try {
					upif.setMaximum(true);
				} catch (Exception ex) {
					ex.getMessage();
				}
			}
		});
		mnUsurios.add(mntmPerfil);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JDesktopPane jdpMain = new JDesktopPane();
		jdpMain.setBackground(SystemColor.activeCaptionText);
		jdpMain.setBounds(12, 535, 776, -526);
		contentPane.add(jdpMain);
		jdpMain.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));		
	}
}
