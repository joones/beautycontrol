package br.com.sis.beautycontrol.views;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import br.com.sis.beautycontrol.core.dao.CostumerDAO;
import br.com.sis.beautycontrol.core.model.CostumerModel;

@SuppressWarnings("serial")
public class NewCustomerInternalFrame extends JInternalFrame {
	private JTextField jtfName;
	private JTextField jtfEmail;
	private JTextField jtfPhone;
	private JTextField jtfCellphone;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewCustomerInternalFrame frame = new NewCustomerInternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NewCustomerInternalFrame() {
		setClosable(true);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setTitle("Cadastrar cliente");
		setBounds(100, 100, 497, 277);
		setFrameIcon(null);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(22, 10, 43, 14);
		getContentPane().add(lblNome);
		
		jtfName = new JTextField();
		jtfName.setBounds(22, 29, 317, 27);
		getContentPane().add(jtfName);
		jtfName.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(22, 76, 40, 14);
		getContentPane().add(lblEmail);
		
		jtfEmail = new JTextField();
		jtfEmail.setBounds(22, 90, 317, 27);
		getContentPane().add(jtfEmail);
		jtfEmail.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setBounds(22, 139, 58, 14);
		getContentPane().add(lblTelefone);
		
		jtfPhone = new JTextField();
		jtfPhone.setBounds(22, 153, 141, 27);
		getContentPane().add(jtfPhone);
		jtfPhone.setColumns(10);
		
		JLabel lblCelular = new JLabel("Celular:");
		lblCelular.setBounds(184, 139, 49, 14);
		getContentPane().add(lblCelular);
		
		jtfCellphone = new JTextField();
		jtfCellphone.setBounds(184, 153, 155, 27);
		getContentPane().add(jtfCellphone);
		jtfCellphone.setColumns(10);
		
		JButton btnLimparCampos = new JButton("Limpar Campos");
		btnLimparCampos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				jtfName.setText("");
				jtfEmail.setText("");
				jtfPhone.setText("");
				jtfCellphone.setText("");
			}
		});
		btnLimparCampos.setBounds(130, 191, 127, 35);
		getContentPane().add(btnLimparCampos);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				CostumerDAO costumerDAO     = new CostumerDAO();
				CostumerModel costumerModel = new CostumerModel();
				
				costumerModel.setName(jtfName.getText());
				costumerModel.setEmail(jtfEmail.getText());
				costumerModel.setPhone(jtfPhone.getText());
				costumerModel.setCellphone(jtfCellphone.getText());
			    costumerDAO.create(costumerModel);
			    
			    jtfName.setText("");
			    jtfEmail.setText("");
			    jtfPhone.setText("");
			    jtfCellphone.setText("");
			}
		});
		btnCadastrar.setBounds(22, 191, 98, 35);
		getContentPane().add(btnCadastrar);
	}
}
