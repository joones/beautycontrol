package br.com.sis.beautycontrol.views;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import br.com.sis.beautycontrol.core.dao.ServiceDAO;
import br.com.sis.beautycontrol.core.model.ServiceModel;

@SuppressWarnings("serial")
public class SearchServiceInternalFrame extends JInternalFrame {
	private JTable tableService;
	private DefaultTableModel tableModel = new DefaultTableModel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchServiceInternalFrame frame = new SearchServiceInternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SearchServiceInternalFrame() {
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setClosable(true);
		setTitle("Consultar servi\u00E7o");
		setFrameIcon(null);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(36, 25, 730, 204);
		getContentPane().add(scrollPane);
		
		ServiceDAO serviceDAO = new ServiceDAO();
		
		tableService = new JTable(tableModel);
		tableModel.addColumn("Servi�o");
		tableModel.addColumn("Valor");
		
		tableService.getColumnModel().getColumn(0).setPreferredWidth(550);
		tableService.getColumnModel().getColumn(1).setPreferredWidth(150);
		
		tableModel.setNumRows(0);
		
		for (ServiceModel serviceModel : serviceDAO.read()){
			tableModel.addRow(new Object[] { serviceModel.getName(), serviceModel.getPrice() });
		}
		
		scrollPane.setViewportView(tableService);
			
//		tableService = new JTable();
//		scrollPane.setViewportView(tableService);

	}

}
