package br.com.sis.beautycontrol.views;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import br.com.sis.beautycontrol.core.dao.AgendaDAO;
import br.com.sis.beautycontrol.core.model.AgendaModel;

@SuppressWarnings("serial")
public class SearchAppointmentInternalFrame extends JInternalFrame {
	private JTable tableAgenda;
	private DefaultTableModel tableModel = new DefaultTableModel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchAppointmentInternalFrame frame = new SearchAppointmentInternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SearchAppointmentInternalFrame() {
		setClosable(true);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setTitle("Consultar hor\u00E1rios");
		setBounds(100, 100, 450, 300);
		setFrameIcon(null);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(24, 24, 720, 223);
		getContentPane().add(scrollPane);
		
		AgendaDAO agendaDAO = new AgendaDAO();
		
		tableAgenda = new JTable(tableModel);
		tableModel.addColumn("Id Cliente");
		tableModel.addColumn("Data");		
		tableModel.addColumn("Hora");
		tableModel.addColumn("Status");
		
		tableAgenda.getColumnModel().getColumn(0);
		tableAgenda.getColumnModel().getColumn(1);
		tableAgenda.getColumnModel().getColumn(2);
		tableAgenda.getColumnModel().getColumn(3);
		
		tableModel.setNumRows(0);
		
		for (AgendaModel agendaModel : agendaDAO.read()) {
			tableModel.addRow(new Object[] { agendaModel.getCostumerId(), agendaModel.getServiceDate(), agendaModel.getServiceHour(), agendaModel.getStatus() });
		}
		
		scrollPane.setViewportView(tableAgenda);
	}
}
