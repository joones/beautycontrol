package br.com.sis.beautycontrol.views.report;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.border.LineBorder;
import java.awt.Color;

@SuppressWarnings("serial")
public class NewReportInternalFrame extends JInternalFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewReportInternalFrame frame = new NewReportInternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NewReportInternalFrame() {
		setTitle("Novo relat\u00F3rio");
		setFrameIcon(null);
		setClosable(true);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBounds(100, 100, 450, 300);

	}

}
