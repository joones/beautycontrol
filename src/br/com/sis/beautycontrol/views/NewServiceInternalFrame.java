package br.com.sis.beautycontrol.views;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import br.com.sis.beautycontrol.core.dao.ServiceDAO;
import br.com.sis.beautycontrol.core.model.ServiceModel;

@SuppressWarnings("serial")
public class NewServiceInternalFrame extends JInternalFrame {
	private JTextField jtfService;
	private JTextField jtfPrice;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewServiceInternalFrame frame = new NewServiceInternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NewServiceInternalFrame() {
		setFrameIcon(null);
		setClosable(true);
		setTitle("Cadastrar Servi\u00E7o");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblServio = new JLabel("Servi\u00E7o:");
		lblServio.setBounds(10, 11, 61, 14);
		getContentPane().add(lblServio);
		
		jtfService = new JTextField();
		jtfService.setBounds(10, 27, 195, 27);
		getContentPane().add(jtfService);
		jtfService.setColumns(10);
		
		JLabel lblValor = new JLabel("Valor:");
		lblValor.setBounds(10, 65, 39, 14);
		getContentPane().add(lblValor);
		
		jtfPrice = new JTextField();
		jtfPrice.setBounds(10, 79, 93, 27);
		getContentPane().add(jtfPrice);
		jtfPrice.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ServiceDAO serviceDAO = new ServiceDAO();
				ServiceModel serviceModel = new ServiceModel();
				
				serviceModel.setName(jtfService.getText());
				serviceModel.setPrice(Double.parseDouble(jtfPrice.getText()));
				jtfPrice.setText(jtfPrice.toString());
				
				serviceDAO.create(serviceModel);
				
				jtfService.setText("");
				jtfPrice.setText("");
			}
		});
		btnCadastrar.setBounds(10, 117, 93, 23);
		getContentPane().add(btnCadastrar);
		
		JButton btnLimparcampos = new JButton("LimparCampos");
		btnLimparcampos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				jtfService.setText("");
				jtfPrice.setText("");
			}
		});
		btnLimparcampos.setBounds(113, 117, 128, 23);
		getContentPane().add(btnLimparcampos);

	}
}
